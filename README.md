# reemc_docker_temporary

Temporary Docker Image for development with the REEM-C.

### To use:

1. `docker pull fjandrad/reemc-waterloo:latest` (you'll need permission to do this)
2. `make`
3. `./start.sh` to start the container (you can open new terminals and run this as many times as you like to get multiple shells)


### Closing the Docker container
1. To exit the container, type ``exit`` on the first terminal that you spawned the container on or open a new terminal window (not inside the container) and type ``docker kill "container_name"`` where "container_name" is the container name on your system. (To find the container name type ``docker images``)


### Notes:

user: docker, pw: docker

The folder you run ./start.sh from will get mounted inside the container so any changes you make there will be persistent even when you exit the container. Just go to the home directory, find your username and cd into that directory. Or, an 'exchange' folder is created in the home directory similar to the PAL docker image where you can store files.